	$(document).ready(function(){
		surveyDisplay();
	});
	

$(function () {
  
	/* Perform Iframe Hack Checks */
	if($('body').hasClass('d-none')) {
    	if (self == top) {
            $('body').removeClass('d-none');
        } else {
            top.location = self.location;
        }
	}
	
	/* IE support */
	var dropdownTab = $('.dropdown-tab');
	if(dropdownTab.length > 0) {
		$('.glyphicon').addClass('fa fa-2x');
        updateSelectMenu();
    }
	
	//Dropdown Menu - auto close on selecting child item
	var dropdownItem = $('.dropdown-item');
	dropdownItem.click(function(){
		$(this).closest('.dropdown').find('.dropdown-toggle').trigger('click');
	});
  

	//base anchors
	setBaseAnchors(); 
	$(".nav-border button i").click(function(){
		$(this).toggleClass("fa-plus-circle fa-minus-circle");
	});

	var baseAnchors = $('.base-anchors');
	if(!$('body').hasClass('main') && baseAnchors.length > 0 && baseAnchors.find('.navbar-nav a').size() == 0) {
		baseAnchors.hide();
	}
	
	$(window).scroll(function() {
		setBaseAnchors();
	});
	
    $(window).resize(function(){
        setBaseAnchors();
    });

	$(".base-anchors .navbar-nav a[href^='#']").on('click', function(e) {
		e.preventDefault();
		var hash = this.hash;
		$(this).parent().find('.active').removeClass('active');
        $(this).addClass('active');
		$('html, body').animate({
			scrollTop: $(hash).offset().top
		}, 300, 'linear');

		if($(window).width() < 767) {
			$('.base-anchors .nav-border span').click();
			$(".base-anchors .navbar span").text($(".base-anchors .navbar a.active").text());
		}
	});

	$('.base-anchors .nav-border span').click(function(){
		$('.base-anchors .navbar-toggler').click();
		$(".nav-border button i").toggleClass("fa-plus-circle fa-minus-circle");
	});

	
  $("body").on("click", ".linked-nav .nav-link", function() {
      var $this = $(this);

      var $navItem = $this.parents(".nav-item");
      var index = $navItem.index();

      var $nav = $this.parents(".linked-nav");
      var $dropdown = $nav.siblings(".linked-dropdown");

      var $dropdownItem = $dropdown.find(".dropdown-item").eq(index);
      $dropdownItem.addClass("active").siblings().removeClass("active");

      $dropdown.find(".dropdown-toggle").text($dropdownItem.text());
  });

  $("body").on("click", ".linked-dropdown .dropdown-item", function() {
      var $this = $(this);

      $this.addClass("active").siblings().removeClass("active");
      var id = $this.data("id");
      $(id).addClass("active").siblings(".tab-pane").removeClass("active");

      var $dropdown = $this.parents(".dropdown");
      $dropdown.removeClass("open");
      $dropdown.find(".dropdown-toggle").text($this.text());

      var index = $this.index();

      var $navTabs = $dropdown.siblings(".linked-nav");
      var $navItem = $navTabs.find(".nav-item").eq(index);
      $navItem.find(".nav-link").addClass("active")
      $navItem.siblings().find(".nav-link").removeClass("active");

      return false;
  });
  
  $(".filterBy-mobile").on("click", function(e) {
	  e.preventDefault();
	  var $this = $(this);
	  var filterArea =  $(".filterBy .row");
	  
	  $this.toggleClass("collapsed");
	  $(filterArea).slideToggle();
  });
  
  $('#termsaccept').click(function(e) {
	    e.preventDefault();
		var termsCookieUrl = $(this).attr('data-url');
		$.ajax({
			 type: "POST",		
			 url : termsCookieUrl,
			 success: function(data) {
				 $('#termscookie').hide();
			 }
		});
	});

  	$('#subscribeBtn').click(function(e) {
		  var subscription = $("#subscription");
		  var email = $("#email").val();
		  var subscriptionMessage = $("#subscriptionMessage");
		  subscriptionMessage.html($('.spinner').html());
		  if(email.length > 0) {
			$.ajax({
			  type: "POST",
			  url : subscription.attr("action"), 
			  data: subscription.serialize(),
				  success: function() {
					  subscriptionMessage.html($('#dataSuccess').html());
					  $('#email').prop('disabled', true);
					  $('#subscribeBtn').prop('disabled', true);
				  },
			      error: function() {
					  subscriptionMessage.html($('#dataFail').html());
		          }
			});
		  } else {
			  subscriptionMessage.html($('#dataFail').html());
		  }
  	});

  /* social */
  var socialElem = $(".social");
  var socialButton = $(".social .dropdown-toggle");
  var socialMenu = $(".social .dropdown-menu");
  
  $(socialButton).click(function(){
	  $(socialMenu).toggle();
  })
  if($('.main .home-wrapper').length > 0) {
	  $('.social').hide();
  }
  
  if($('.content-inner .table-bottom-links').length > 0) {
	  socialElem.prependTo(".table-bottom-links");
  }else if($('.content-inner .awards-details').length > 0) {
 	  socialElem.prependTo(".awards-details");
  }else if($('.container.page-title').length > 0) {
 	  socialElem.insertBefore(".container.page-title > h1");
  }else{
  	  socialElem.insertAfter(".content-inner:nth-child(1) > h1");
  }
});

function surveyDisplay() {
	var surveryHolder = $(".survey-holder");
	var surveyClose = surveryHolder.find(".close-icon");
	surveyClose.click(function(){
		surveryHolder.fadeOut('fast');
	});
    setTimeout(function(){
    	surveryHolder.show(); 
    }, 30000);
};

function setBaseAnchors() {
		var baseAnchors = $('.base-anchors');
		if(baseAnchors.length > 0) {
			var scroll = $(window).scrollTop();
			var scrollValue = 220;
			var position = 0;

			if ($(window).width() < 767) {
				scrollValue = 200;
			}

			if (scroll >= scrollValue) {
				baseAnchors.addClass("fixed-top");
			} else {
				baseAnchors.removeClass("fixed-top");
				var baseContent = $( ".base-content:first");
				if(baseContent.length > 0) {
					var topPosition = baseContent.position();
					position = topPosition.top - scroll;
				}
			}
			baseAnchors.css('top', position);
		}
};

function updateSelectMenu() {
	var tabsSelect = $(".data-tabs-list li a.active");
    if(tabsSelect.length > 0) {
        tabsSelect.parents('li').addClass('active');
    }
    
	var dropdownSelectLink = $(".data-tabs-list a");
	if(dropdownSelectLink.length > 0) {
	    var dropdownList = $('.data-tabs-list li.active');
	    if(dropdownList.length > 0){
	        var selectTitle = dropdownList.find('a').text();
	        $('.dropdown-tab .select-title').text(selectTitle);
	    }
	}
}