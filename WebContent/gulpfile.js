'use strict';
 
var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var copy = require('gulp-copy');
var stylelint = require("gulp-stylelint");
var duration = require('gulp-duration')

gulp.task('lint-css', function(){
	return gulp.src(['./app/scss/pages/*.scss'])
	.pipe(stylelint({
        failAfterError: true,
        reporters: [{ formatter: "string", console: true }]
    }));
});

gulp.task('sass', function () {
  gulp.src(['./app/scss/ap.scss'])
    .pipe(sass({
        outputStyle: 'expanded'
    }))
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
});
 
gulp.task('js', function(){
    return gulp.src(['./app/js/pages/global.js'])
    .pipe(concat('ap.js'))
    //.pipe(uglify({compress: false, preserveComments: 'all', mangle: false}))
    .pipe(gulp.dest('./dist/js/'));
});

gulp.task('fa', function(){
    return gulp.src('./app/fonts/**')
    .pipe(copy('./dist/', {
        prefix: 1
    }));
});

gulp.task('exit', function () {
    //process.exit(0);
});

gulp.task('intervalExit', function() {
  setInterval(function() {
    process.exit(0);
  }, 5000)
})

gulp.task('watch', function () {
    gulp.watch(['./app/scss/**/*.scss'], ['sass']);
    gulp.watch(['./app/scss/pages/*.scss'], ['lint-css']);
    gulp.watch(['./app/js/**/*.js'], ['js']);
});

gulp.task('default', ['lint-css', 'fa', 'sass', 'js', 'watch', 'intervalExit']); 